CREATE TABLE `documento` (
  `codigo` varchar(500) NOT NULL,
  `data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titulo` varchar(500) NOT NULL,
  `departamento` varchar(500) NOT NULL,
  `categoria` varchar(500) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
