module.exports = (app) => {
    app.get('/documento/lista', (req, resp) => {
        var connection = app.persistencia.connectionFactory();
        var documentoDAO = new app.persistencia.documentoDAO(connection);
        var lista = documentoDAO.lista(function (erro, res) {
            if (erro) {
                res.status(500).json("Erro na consulta!", erro);
            }
            else {
                resp.status(200).json(res);
            }
        });

        connection.end();
    });

    app.post('/documento/salva', function (req, res) {
        /**
         * Validação da requisição
         */
        req.assert("data.codigo", "Campo Código é obrigatório na requisição!").notEmpty();
        req.assert("data.departamento", "Campo Departamento é obrigatório na requisição").notEmpty();
        req.assert("data.titulo", "Campo Título obrigatório na requisição").notEmpty();
        req.assert("data.categoria", "Campo categoria é obrigatório na requisição").notEmpty();



        var errosEncontrados = req.validationErrors();

        if (errosEncontrados) {
            console.log('Erros de validação encontrados');
            res.status(400).send("Preencha Todos os campos");
        }

        var documento = req.body.data;
        var connection = app.persistencia.connectionFactory();
        var documentoDAO = new app.persistencia.documentoDAO(connection);

        documentoDAO.salva(documento, function (erro, resultado) {
            if (erro) {
                console.log('Erro ao inserir no banco.', erro);
                res.status(500).send(erro);
            }
            else {
                console.log('documento salvo com sucesso');
                res.status(200).send();
            }
        });
    });
};


