var express = require('express');
var consign = require('consign');
var cors = require('cors')
var expressValidator = require('express-validator');
var load = require('express-load');
var bodyParser = require('body-parser');


module.exports = function () {

    var app = express();

    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(expressValidator());



    consign()
        .include('routes')
        .then('persistencia')
        .into(app);

    app.use(bodyParser.urlencoded({ extended: true }));

    return app;
};