function documentoDAO(connection) {
    this._connection = connection;
}

documentoDAO.prototype.lista = function (callback) {
    this._connection.query(`select codigo, DATE_FORMAT(data_cadastro, "%d/%m/%Y") as data_cadastro,titulo,
                            departamento,categoria from documento order by titulo asc`, callback);
};

documentoDAO.prototype.salva = function (documento, callback) {
    this._connection.query('insert into documento set ?', documento, callback);
}

module.exports = () => {
    return documentoDAO;
};

